import com.mysql.jdbc.StringUtils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main3 {
    // mysql驱动包名
    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    // 数据库连接地址
    private static final String URL = "jdbc:mysql://192.168.101.101:3306/dmp";
    // 用户名
    private static final String USER_NAME = "dmp_root";
    // 密码
    private static final String PASSWORD = "dmp_913.Net";

//    public static int channelId = 1000000;

    public static String generateChannelId()
    {
        String chars = "0123456789";
        char[] rands = new char[7];
        for (int i = 0; i < 7; i++)
        {
            int rand = (int) (Math.random() * 10);
            rands[i] = chars.charAt(rand);
        }
        return new String(rands);
    }



    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }


    public static void main(String[] args) {
        Connection connection = null;
        PreparedStatement prst = null;
        try {
            // 注册mysql驱动
            Class.forName(DRIVER_NAME);
            // 获取数据库连接对象
            connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
            connection.setAutoCommit(false);


            String q_sql = "select DISTINCT channel_id from live_channel_setting where user_id = '8e15606eab'";
            prst = connection.prepareStatement(q_sql);
            ResultSet rs = prst.executeQuery();
            List<String> channnelList = new ArrayList<>(583159);
            while (rs.next()) {
                channnelList.add(rs.getString(1));
            }


            StringBuilder sql = new StringBuilder("replace into live_channel_metric_test values ");
            Random random = new Random();
            Random r_index = new Random();
            int p=1;
            // 造100w的数据
            while (p<=1000) {
                for (int batch = 1; batch<=100; batch++) {
                    int index = r_index.nextInt(583159);
                    String channelId = channnelList.get(index);
                    for (int i = 0; i <= 9; i++) {
                        sql.append(" ('2021-07-21 00:00:00', '8e15606eab',");
                        sql.append("'").append(channelId).append("',")
                                .append(i).append(",")
                                .append(random.nextInt(1000)).append("),");
                    }
                }
                prst = connection.prepareStatement(sql.substring(0, sql.length()-1));
                prst.addBatch();
                Long start = System.currentTimeMillis();
                System.out.println("开始写入数据时间： " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(start)));
                prst.executeBatch();
                connection.commit();
                System.out.println("已写入数据： " + p*1000);
                System.out.println("耗时s： " + (System.currentTimeMillis()-start)/1000);
                p++;
                sql = new StringBuilder("replace into live_channel_metric_test values ");
            }




//            // 查询
//            String sql = "select DISTINCT channel_id from live_channel_setting where user_id = '8e15606eab' limit 0,100000";
//            prst = connection.prepareStatement(sql);
//            ResultSet rs = prst.executeQuery();
//
//            StringBuilder sb = new StringBuilder(1000000);
//            while (rs.next()) {
//                String cid = rs.getString(1);
//                sb.append(cid).append(",");
////                sb.append("'").append(cid).append("'").append(",");
//            }
////            System.out.println(sb.substring(0, sb.length()-1));
//
//
//
//            /**
//             * 写文件
//             */
//            File file = null;
//            FileWriter fw = null;
//            file = new File("C:\\Users\\T470\\Desktop\\test1.txt");
//            if (!file.exists()) {
//                file.createNewFile();
//            }
//            fw = new FileWriter(file);
//            fw.write(sb.substring(0, sb.length()-1));
//            fw.flush();
//            System.out.println("写数据成功！");
//            fw.close();

//
//            String url = "http://localhost:8799/live/backstage/statistics/channel/list?userId=8e15606eab";
//            String initParam = "startTimeStamp=1626278400000&endTimeStamp=1626537600000";
//
//            StringBuilder requestParam = new StringBuilder(1000000);
//            requestParam.append(initParam).append("&channels=").append(sb.substring(0, sb.length()-1));
//
//            Long stp = System.currentTimeMillis();
//            System.out.println(sendPost(url, requestParam.toString()));
//            Long etp = System.currentTimeMillis();
//            System.out.println("耗时s： " + (etp-stp)/1000);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    prst.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

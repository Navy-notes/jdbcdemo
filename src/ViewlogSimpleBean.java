public class ViewlogSimpleBean {
    private String channelId;
    private int playDuration;
    private String sessionId;
    private String param1;
    private String param2;
    private String param3;
    private String browser;
    private String isMobile;
    private String currentDay;
    private String createdTime;
    private Integer extraMinuteDuration;   // 冗余分钟数

    public ViewlogSimpleBean(String channelId, int playDuration, String sessionId, String param1, String param2, String param3, String browser, String isMobile, String currentDay, String createdTime) {
        this.channelId = channelId;
        this.playDuration = playDuration;
        this.sessionId = sessionId;
        this.param1 = param1;
        this.param2 = param2;
        this.param3 = param3;
        this.browser = browser;
        this.isMobile = isMobile;
        this.currentDay = currentDay;
        this.createdTime = createdTime;
    }

    public ViewlogSimpleBean(String channelId, String param1, String sessionId) {
        this.channelId = channelId;
        this.param1 = param1;
        this.sessionId = sessionId;
    }

    public ViewlogSimpleBean(String channelId, String param1) {
        this.channelId = channelId;
        this.param1 = param1;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public int getPlayDuration() {
        return playDuration;
    }

    public void setPlayDuration(int playDuration) {
        this.playDuration = playDuration;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(String isMobile) {
        this.isMobile = isMobile;
    }

    public String getCurrentDay() {
        return currentDay;
    }

    public void setCurrentDay(String currentDay) {
        this.currentDay = currentDay;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public Integer getExtraMinuteDuration() {
        return extraMinuteDuration;
    }

    public void setExtraMinuteDuration(Integer extraMinuteDuration) {
        this.extraMinuteDuration = extraMinuteDuration;
    }
}

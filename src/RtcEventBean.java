import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RtcEventBean {
    private String roomId;
    private String rtcUid;
    private String event;
    private Long eventTime;

    public RtcEventBean() {
    }

    public RtcEventBean(String roomId, String rtcUid, String event, String eventTime) throws ParseException {
        this.roomId = roomId;
        this.rtcUid = rtcUid;
        this.event = event;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        this.eventTime = sdf.parse(eventTime).getTime()/1000;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRtcUid() {
        return rtcUid;
    }

    public void setRtcUid(String rtcUid) {
        this.rtcUid = rtcUid;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Long getEventTime() {
        return eventTime;
    }

    public void setEventTime(Long eventTime) {
        this.eventTime = eventTime;
    }
}

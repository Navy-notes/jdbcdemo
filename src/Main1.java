import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main1 {
    // mysql驱动包名
    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    // 数据库连接地址
    private static final String URL = "jdbc:mysql://192.168.1.7:3306/test";
    // 用户名
    private static final String USER_NAME = "analytics_root";
    // 密码
    private static final String PASSWORD = "analytics_913.NET";


    public static void main(String[] args) {

        // 进房时间结果集
        List<RtcEventBean> joinList = new ArrayList<>();
        // 退房时间结果集
        List<RtcEventBean> leaveList = new ArrayList<>();
        // 合并输出
        List<RtcCollectBean> resultList = new ArrayList<>();

        Connection connection = null;
        try {
            // 注册mysql驱动
            Class.forName(DRIVER_NAME);
            // 获取数据库连接对象
            connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
            // 创建sql执行对象；mysql查询语句
            String sql = "SELECT roomId, rtcUid, event, eventTime\n" +
                    "FROM trtc_event_record\n" +
                    "WHERE rtcUid= ? and eventTime >= ?  and eventTime < ? and `event`= ? \n" +
                    "ORDER BY eventTime ASC";
            // 执行sql命令，返回结果集
            PreparedStatement prst = connection.prepareStatement(sql);
            prst.setString(1,"1624067136887");  // rtcUid
            prst.setString(2, "2021-06-19 00:00:00");  // startDate
            prst.setString(3, "2021-06-20 00:00:00");  // endDate

            // 进房事件，升序排序
            prst.setString(4,"EVENT_TYPE_ENTER_ROOM");  // 进房事件
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                joinList.add(new RtcEventBean(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }

            // 退房事件，升序排序
            prst.setString(4,"EVENT_TYPE_EXIT_ROOM");  // 退房事件
            rs = prst.executeQuery();
            while (rs.next()) {
                leaveList.add(new RtcEventBean(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            rs.close();
            prst.close();

            // 保证同一对进退房时间中，离开时间大于等于进入时间，否则作为脏数据单独先预处理
            for (int i=0; i<joinList.size(); i++) {
                if (i<leaveList.size()) {
                    if (joinList.get(i).getEventTime() <= leaveList.get(i).getEventTime())
                        continue;
                    else {
                        leaveList.remove(i);  // 对退房时间无法对齐进房时间的脏数据进行排除
                        i--;    // 依然用join时间判断下一个leave时间
                    }
                }
            }

            // 合并逻辑
            int joinSize = joinList.size();
            int leaveSize = leaveList.size();
            if (joinSize == leaveSize) {
                // 两两配对
                for (int i=0; i<joinSize; i++) {
                    RtcEventBean joinEventBean = joinList.get(i);
                    RtcEventBean leaveEventBean = leaveList.get(i);
                    resultList.add(new RtcCollectBean(joinEventBean.getRoomId(), joinEventBean.getRtcUid(), joinEventBean.getEventTime(), leaveEventBean.getEventTime()));
                }
            }
            else if (joinSize > leaveSize) {
                for (int i=0; i<joinSize; i++) {
                    // 先根据进房时间和退房时间两两配对
                    RtcEventBean joinEventBean = joinList.get(i);
                    if (i<leaveSize) {
                        RtcEventBean leaveEventBean = leaveList.get(i);
                        resultList.add(new RtcCollectBean(joinEventBean.getRoomId(), joinEventBean.getRtcUid(), joinEventBean.getEventTime(), leaveEventBean.getEventTime()));
                    }
                    // 对多余的进房时间判断是否小于等于最晚的退房时间，如果是则舍弃不处理，不是的话则在该进房时间的基础上补一分钟
                    else {
                        if (joinEventBean.getEventTime() > leaveList.get(leaveSize-1).getEventTime()) {
                            resultList.add(new RtcCollectBean(joinEventBean.getRoomId(), joinEventBean.getRtcUid(), joinEventBean.getEventTime(), joinEventBean.getEventTime()+59));
                        }
                    }
                }
            }
            else {
                // 1. 对于进房事件为0的情况，直接在每个退房时间的基础上减去一分钟得到进房时间
                if (joinSize == 0) {
                    for (RtcEventBean e: leaveList) {
                        resultList.add(new RtcCollectBean(e.getRoomId(), e.getRtcUid(), e.getEventTime()-59, e.getEventTime()));
                    }
                }
                else {
                    // 2. 进房事件非0情况
                    for (int i=0; i<leaveSize; i++) {
                        RtcEventBean leaveEventBean = leaveList.get(i);
                        // 先根据退房时间和进房时间两两配对
                        if (i<joinSize) {
                            RtcEventBean joinEventBean = joinList.get(i);
                            resultList.add(new RtcCollectBean(leaveEventBean.getRoomId(), leaveEventBean.getRtcUid(), joinEventBean.getEventTime(), leaveEventBean.getEventTime()));
                        }
                        // 然后对多余的退房时间与最晚的进房时间作差，判断差值是否小于一分钟，小于则将该进房时间作为配对，若不小于则将该退房时间减去一分钟得到进房时间。
                        else {
                            RtcEventBean lastJoinEventBean = joinList.get(joinSize-1);
                            if (leaveEventBean.getEventTime() - lastJoinEventBean.getEventTime() < 59) {
                                resultList.add(new RtcCollectBean(leaveEventBean.getRoomId(), leaveEventBean.getRtcUid(), lastJoinEventBean.getEventTime(), leaveEventBean.getEventTime()));
                            }
                            else {
                                resultList.add(new RtcCollectBean(leaveEventBean.getRoomId(), leaveEventBean.getRtcUid(), leaveEventBean.getEventTime()-59, leaveEventBean.getEventTime()));
                            }
                        }
                    }
                }
            }


            // 输出结果
            System.out.println("room_id" + "\t\t" + "uid" + "\t\t" + "join_time" + "\t\t" + "leave_time");
            for (RtcCollectBean e: resultList) {
                System.out.print(e.getRoomId() + "\t");
                System.out.print(e.getUid() + "\t");
                System.out.print(e.getJoinTime() + "\t");
                System.out.print(e.getLeaveTime() + "\t");
                System.out.println();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

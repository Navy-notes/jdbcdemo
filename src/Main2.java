import com.mysql.jdbc.StringUtils;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Main2 {
    // mysql驱动包名
    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    // 数据库连接地址
    private static final String URL = "jdbc:mysql://192.168.1.7:3306/test";
    // 用户名
    private static final String USER_NAME = "analytics_root";
    // 密码
    private static final String PASSWORD = "analytics_913.NET";


    public static void main(String[] args) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        List<ViewlogSimpleBean> param1List = new ArrayList<>();


        for (int day = 01; day <= 22; day++) {
            /**
             * 控制日期传参，按天处理
             */
            String date = "2021-04-" + String.format("%02d", day);
            System.out.println(date);  // 相当于打印日志的作用，还能查看当前处理进度

            Connection connection = null;
            try {

                // 注册mysql驱动
                Class.forName(DRIVER_NAME);
                // 获取数据库连接对象
                connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
                // 创建sql执行对象；mysql查询语句
                String sql = "select channelId,param1\n" +
                        "FROM viewlog_test\n" +
                        "WHERE currentDay=? \n" +
                        "GROUP BY channelId,param1\n" +
                        "HAVING COUNT(1)>1";
                // 执行sql命令，返回结果集
                PreparedStatement prst = connection.prepareStatement(sql);
                prst.setString(1, date);  // 传参

                // 查询viewlog记录大于1的观众ID
                ResultSet rs = prst.executeQuery();
                while (rs.next()) {
                    String aId = rs.getString(2);
                    if (!StringUtils.isNullOrEmpty(aId)) {
                        param1List.add(new ViewlogSimpleBean(rs.getString(1), rs.getString(2)));
                    }
                }
                rs.close();
                prst.close();

                // 频道ID、观众ID、场次作为传参
                sql = "SELECT channelId,playDuration,sessionId,param1,param2,param3,browser,isMobile,currentDay,createdTime\n" +
                        "FROM viewlog_test\n" +
                        "WHERE channelId=? and param1=? and currentDay=?\n" +
                        "ORDER BY createdTime ASC";

                for (ViewlogSimpleBean e : param1List) {

//                    if (StringUtils.isNullOrEmpty(e.getSessionId())) {
//                        prst = connection.prepareStatement(sql.replace("sessionId=?", " sessionId is null "));
//                        prst.setString(1, e.getChannelId());
//                        prst.setString(2, e.getParam1());
//                        prst.setString(3, date);
//                    } else {
//                        prst = connection.prepareStatement(sql);
//                        prst.setString(1, e.getChannelId());
//                        prst.setString(2, e.getParam1());
//                        prst.setString(3, e.getSessionId());
//                        prst.setString(4, date);
//                    }
                    prst = connection.prepareStatement(sql);
                    prst.setString(1, e.getChannelId());
                    prst.setString(2, e.getParam1());
                    prst.setString(3, date);
                    rs = prst.executeQuery();  // 执行查询

                    List<ViewlogSimpleBean> list = new ArrayList<>();
                    // 取结果集
                    while (rs.next()) {
                        list.add(new ViewlogSimpleBean(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5),
                                rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10)));
                    }
                    rs.close();
                    prst.close();

//                // 取第二条记录的createTime与第一条记录的createTime得到一个按秒的时间差
//                Long cTime1 = sdf.parse(list.get(1).getCreatedTime()).getTime()/1000;
//                Long cTime0 = sdf.parse(list.get(0).getCreatedTime()).getTime()/1000;
//                // 然后与第一条记录的playDuration相比
//                Long pd0 = Long.valueOf(list.get(0).getPlayDuration());
//                if (Math.abs(pd0 - (cTime1 - cTime0)) > 100) {
//                // 如果两者相差大于100秒，则初步判定为异常观看记录，然后根据重叠时间计算冗余分钟数

                    // 当下账期没有数据的，则中止往下执行
                    if (list.size() > 0) {
                        int extraMinuteDuration = 0;
                        for (int i = 0; i < list.size(); ) {
                            // 计算重叠区间
                            Long sTime = sdf.parse(list.get(i).getCreatedTime()).getTime() / 1000;  // 秒数
                            Long eTime = sTime + list.get(i).getPlayDuration();
                            int j = i + 1;
                            for (; j < list.size(); j++) {
                                Long jCreateTime = sdf.parse(list.get(j).getCreatedTime()).getTime() / 1000;
                                if (jCreateTime < eTime) {
                                    extraMinuteDuration += Math.ceil(list.get(j).getPlayDuration() * 1.0 / 60);  // 向上取整分钟数,并累加
                                } else {
                                    break;
                                }
                            }
                            i = j;
                        }


                        // 将冗余分钟数不为0的数据写入测试库的viewlog_abnormal，ignore忽略主键重复
                        list.get(0).setExtraMinuteDuration(extraMinuteDuration);
                        if (extraMinuteDuration != 0) {
                            Statement st = connection.createStatement();
                            for (ViewlogSimpleBean bean : list) {
                                StringBuilder insertSql = new StringBuilder("insert ignore into viewlog_abnormal(channelId,playDuration,sessionId,param1,param2,param3,browser,isMobile,currentDay,createdTime,extraMinuteDuration) " +
                                        "values (");
                                insertSql.append("'" + bean.getChannelId() + "'");
                                insertSql.append(",");
                                insertSql.append(bean.getPlayDuration());
                                insertSql.append(",");
                                insertSql.append("'" + bean.getSessionId() + "'");
                                insertSql.append(",");
                                insertSql.append("'" + bean.getParam1() + "'");
                                insertSql.append(",");
                                insertSql.append("'" + bean.getParam2() + "'");
                                insertSql.append(",");
                                insertSql.append("'" + bean.getParam3() + "'");
                                insertSql.append(",");
                                insertSql.append("'" + bean.getBrowser() + "'");
                                insertSql.append(",");
                                insertSql.append("'" + bean.getIsMobile() + "'");
                                insertSql.append(",");
                                insertSql.append("'" + bean.getCurrentDay() + "'");
                                insertSql.append(",");
                                insertSql.append("'" + bean.getCreatedTime() + "'");
                                insertSql.append(",");
                                insertSql.append(bean.getExtraMinuteDuration());
                                insertSql.append(")");

                                st.addBatch(insertSql.toString());
                            }
                            st.executeBatch();
                            st.clearBatch();
                            st.close();
                    }

//                    }

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

}

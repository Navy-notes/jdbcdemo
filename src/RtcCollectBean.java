import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RtcCollectBean {
    private String roomId;
    private String uid;
    private Long joinTime;
    private Long leaveTime;

    public RtcCollectBean() {
    }

    public RtcCollectBean(String roomId, String uid, Long joinTime, Long leaveTime) throws ParseException {
        this.roomId = roomId;
        this.uid = uid;
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        this.joinTime = joinTime;
        this.leaveTime = leaveTime;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Long joinTime) {
        this.joinTime = joinTime;
    }

    public Long getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(Long leaveTime) {
        this.leaveTime = leaveTime;
    }
}
